import math
from ._gloabl import is_zero
class Vector:
    def __init__(self,lst):
        self._values = lst
    def __repr__(self):
        """系统调用"""
        return "Vector({})".format(self._values)

    @classmethod
    def zero(cls,dim):
        return cls([0]*dim)
    def __str__(self):
        """用户调用"""
        return "({})".format(", ".join(str(e) for e in self._values))
    def __len__(self):
        """返回向量的长度"""
        return len(self._values)
    def __getitem__(self, item):
        """获取向量的第item个元素"""
        return self._values[item]
    def __iter__(self):
        """返回向量的迭代器"""
        return self._values.__iter__()
    def __add__(self, other):
        """重载魔术方法实现向量的加法"""
        assert len(self) == len(other),"长度超过范围"
        return Vector([a + b for a,b in zip(self,other)])
    def __sub__(self, other):
        """重载魔术方法实现向量的减法"""
        assert len(self) == len(other), "长度超过范围"
        return Vector([a - b for a, b in zip(self, other)])
    def __mul__(self, num):
        """重载魔术方法实现向量的乘法 self * num"""
        return Vector([a * num for a in self])
    def __rmul__(self, num):
        """重载魔术方法实现向量的乘法 num * self"""
        return self * num
    def __pos__(self):
        """重载魔术方法实现向量正号 1 * self """
        return 1 * self
    def __neg__(self):
        """重载魔术方法实现向量负号 -1 * self """
        return -1 * self
    def __truediv__(self, k):
        """重载魔术方法实现向量的除法 1 / k * self"""
        return (1/k) * self
    def norm(self):
        """向量的模"""
        return math.sqrt(sum(e**2 for e in self))
    def normalize(self):
        """向量的单位向量"""
        if is_zero(self.norm()):
            raise ZeroDivisionError("norm is zero")
        return Vector(self._values) / self.norm()

    def dot(self,other):
        """向量的点乘"""
        return sum(a * b for a,b in zip(self,other))

    def underlying_list(self):
        """返回底层向量的列表副本"""
        return self._values[:]

