import numpy as np

if __name__ == "__main__":

    # numpy矩阵的创建
    A = np.array([[1,2],[3,4]])
    print(A)

    # 矩阵的形状
    print(A.shape)

    #矩阵的转置
    print(A.T)

    #获取矩阵的元素  等同于 print(A[1,1])
    print(A[(1,1)])

    #获取矩阵的行
    print(A[0])

    #获取矩阵的列
    print(A[:,0])
    print(A[1,:])

    #矩阵的基本运算
    B = np.array([[5,6],[7,8]])
    print(A + B)
    print(A - B)
    print(A * 10)
    print(10 * A)

    # 矩阵的乘法
    print(A.dot(B))
    # 无意义的矩阵乘法，numpy实现 没有什么含义
    print(A * B)

    p = np.array([10,100])
    print(A.dot(p))

    # 矩阵广播 无其他意义
    #print(A+p)
    #print(A+1)

    #试验
    print(((A.dot(B)).T))
    print((B.T).dot(A.T))

    M1 = np.array([[1,2],[3,4]])
    M2 = np.array([[5,6],[7,8]])
    print(M1.dot(M2))

    #单位矩阵
    T = np.array([[1,2],[3,4]])
    I = np.identity(2)
    print(I.dot(T))
    print(T.dot(I))

    #逆矩阵 AB=I A和B必须是方阵，看看性质
    invA = np.linalg.inv(T)
    print(invA)
    print(invA.dot(T))
    print(T.dot(invA))


