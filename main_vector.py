from playLA.Vector import Vector
if __name__ == "__main__":
    vec = Vector([5,2])
    vec2 = Vector([2,3])
    # print(vec)
    # print(len(vec))
    print("vec[0]={},vec[1]={}".format(vec[0],vec[1]))
    print("{} + {} = {}".format(vec,vec2,vec + vec2))
    print("{} - {} = {}".format(vec,vec2,vec - vec2))
    print("{} * {} = {}".format(vec,5,vec * 5))
    print("+{} = {}".format(vec,+vec))
    print("-{} = {}".format(vec,-vec))

    zero = Vector.zero(2)
    print(zero)
    print("{} + {} = {}".format(vec,zero,vec + zero))

    vec = Vector([5,2])
    print(vec.norm())
    print(vec.normalize())
    print(vec.normalize().norm())

    try:
        print(zero.normalize())
    except ZeroDivisionError:
        print(zero)

    print(vec.dot(vec2))