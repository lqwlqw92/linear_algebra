import numpy as np

if __name__ == "__main__":
    """numpy介绍相关"""

    print(np.__version__)

    lst = [1,2,3]
    lst[0] = "a"
    print(lst)

    #np的基本属性 np array的值必须是同一类型
    vec = np.array([1,2,3])
    vec3 = np.array([4,5,6])
    vec[0] = 1
    print(vec)

    vec2 = np.array(["q","b"])
    print(vec2)

    print(np.zeros(5))
    print(np.ones(5))
    print(np.full(5,20))

    print("size = ",vec.size)
    print(vec[-1:])
    print(vec[1:])
    print(vec[0:1])

    print("{} + {} = {}".format(vec,vec3,vec+vec3))
    print("{} - {} = {}".format(vec,vec3,vec-vec3))
    print("{} * {} = {}".format(vec,vec3,vec*vec3))
    print("{} * {} = {}".format(3,vec3,3*vec3))
    print("{} * {} = {}".format(vec3,3,vec3*3))
    print("{} / {} = {}".format(vec,vec3,vec/vec3))
    print("{}.dot{} = {}".format(vec,vec,vec.dot(vec)))

    #向量的模
    print(np.linalg.norm(vec))
    print(np.linalg.norm(vec /np.linalg.norm(vec)))




