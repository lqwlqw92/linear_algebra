from playLA.LinearSystem import LinearSystem
from playLA.Matrix import Matrix
from playLA.Vector import Vector

if __name__ == "__main__":

    A = Matrix([[1,2,4],[3,7,2],[2,3,3]]);
    b = Vector([7,-11,1])
    linearsystem = LinearSystem(A,b)
    linearsystem.gauss_jordan_elimination()
    linearsystem.fancy_print()

    A1 = Matrix([[1,-1,2,0,3],
                 [-1,1,0,2,-5],
                 [1,-1,4,2,4],
                 [-2,2,-5,-1,-3]])
    b = Vector([1,5,13,-1])
    ls = LinearSystem(A1,b)
    ls.gauss_jordan_elimination()
    ls.fancy_print()

    A2 = Matrix([[2,2],[2,1],[1,2]])
    b = Vector([3,2.5,7])
    ls = LinearSystem(A2, b)
    ls.gauss_jordan_elimination()
    ls.fancy_print()
