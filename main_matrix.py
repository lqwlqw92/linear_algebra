from playLA.Matrix import Matrix
from playLA.Vector import Vector
if __name__ == "__main__":

    matrix = Matrix([[1,2],[3,4]])
    # print(matrix)
    # print(matrix.share())
    # print(matrix.row_num())
    # print(matrix.col_num())
    # print(matrix.size())
    # print(matrix.col_vector(1))
    # print(matrix.row_vector(1))
    # print(matrix[1,1])
    #print(matrix.zero(2,3))
    # matrix1 = Matrix([[1,2],[3,4]])
    # matrix2 = Matrix([[5,6],[7,8]])
    #
    # print(matrix1 + matrix2)
    # print(matrix1 - matrix2)
    # print(2 * matrix1)
    # print( -1 * matrix1)
    # print(matrix1 / 5)

    # p = Vector([5,3])
    # T = Matrix([[1.5,0],[0,2]])
    # print("T.dot(p)={}".format(T.dot(p)))
    #
    # P = Matrix([[0,4,5],[0,0,3]])
    # print("T.dot(P)={}".format(T.dot(P)))

    #验证是否满足交换率
    # A = Matrix([[1,2],[3,4]])
    # B = Matrix([[6,7],[8,9]])
    # print(A.dot(B))
    # print(B.dot(A))
    # print(A)
    # print(A.T())

    T = Matrix([[1,2],[3,4]])
    I = Matrix.identity(2)

    print(I.dot(T))
    print(T.dot(I))






